# -*- coding: utf-8 -*-
import sys

reload(sys)
sys.setdefaultencoding('utf8')

import pandas
import os
from pandasql import sqldf
import re
import numpy
import timeit


def search_pattern(data, pattern, mode):
    result = pattern.search(str(data.values)).group()
    if mode in ['AMR_AF', 'AFR_AF', 'EUR_AF', 'SAS_AF', 'EAS_AF']:
        return result[8:-1]
    elif mode in ['AF']:
        return result[4:-1]


ROOT_COMMON_INPUT = 'input/'
ROOT_INPUT = 'library_ph3'
ROOT_OUTPUT = 'output/'
OLD_DATA_CHROMO_ID = '#Chromosome'
NEW_DATA_CHROMO_ID = 'Chromosome'
DATA_START_POSITION = 'Start_position'
DATA_JOIN_CHROMO_ID = 'CHROM'
DATA_JOIN_START_POSITION = "POS"
DATA_JOIN_ID = "ID"
DATA_JOIN_INFO = "INFO"


def module_1(file_name):
    pysqldf = lambda q: sqldf(q, globals())
    global data, data_join
    af_pattern = re.compile("\;AF\=\d+.?\d*")
    amr_af_pattern = re.compile("\;AMR_AF\=\d+.?\d*")
    afr_af_pattern = re.compile("\;AFR_AF\=\d+.?\d*")
    eur_af_pattern = re.compile("\;EUR_AF\=\d+.?\d*")
    sas_af_pattern = re.compile("\;SAS_AF\=\d+.?\d*")
    eas_af_pattern = re.compile("\;EAS_AF\=\d+.?\d*")
    data = pandas.read_csv("%s" % file_name, delim_whitespace=True, usecols=[OLD_DATA_CHROMO_ID, DATA_START_POSITION],
                           header=0)

    data = data.rename(columns = {OLD_DATA_CHROMO_ID:NEW_DATA_CHROMO_ID})
    print "There are {} different chromosomes".format(data.shape[0])
    # interested_file = ['Chr4_aa.xlsx','Chr4_aa_1.xlsx']
    writer = pandas.ExcelWriter('%soutput.xlsx' % ROOT_OUTPUT, engine='openpyxl')
    for subdir, dirs, files in os.walk(ROOT_INPUT):
        current_index = 0
        # for single_file in interested_file:
        for single_file in files:
            if "Chr" in single_file:
                start_time = timeit.default_timer()
                filepath = subdir + os.sep + single_file
                print single_file
                data_join = pandas.read_excel(filepath,
                                              names=[DATA_JOIN_CHROMO_ID, DATA_JOIN_START_POSITION, DATA_JOIN_ID,
                                                     DATA_JOIN_INFO], header=0)
                request = """SELECT DISTINCT
                                            data_join.%s, data_join.%s, data_join.%s, data_join.%s
                                         FROM
                                            data, data_join
                                         WHERE
                                            (data.%s = data_join.%s) AND (data.%s = data_join.%s)
                                            ;""" % (
                    DATA_JOIN_CHROMO_ID, DATA_JOIN_START_POSITION, DATA_JOIN_ID, DATA_JOIN_INFO, NEW_DATA_CHROMO_ID,
                    DATA_JOIN_CHROMO_ID, DATA_START_POSITION, DATA_JOIN_START_POSITION)
                response = pysqldf(request)
                if response.empty == False:
                    response = response.assign(AF=lambda x: search_pattern(x['INFO'], af_pattern, 'AF'),
                                               AMR_AF=lambda x: search_pattern(x['INFO'], amr_af_pattern, 'AMR_AF'),
                                               AFR_AF=lambda x: search_pattern(x['INFO'], afr_af_pattern, 'AFR_AF'),
                                               EUR_AF=lambda x: search_pattern(x['INFO'], eur_af_pattern, 'EUR_AF'),
                                               SAS_AF=lambda x: search_pattern(x['INFO'], sas_af_pattern, 'SAS_AF'),
                                               EAS_AF=lambda x: search_pattern(x['INFO'], eas_af_pattern, 'EAS_AF'))
                    del response['INFO']
                    print response
                    if current_index != 0:
                        response.to_excel(excel_writer=writer, startrow=current_index, header=False, index=False)
                        current_index += response.shape[0]
                    else:
                        response.to_excel(excel_writer=writer, startrow=current_index, header=True, index=False)
                        current_index += response.shape[0]
                        current_index += 1
                end_time = timeit.default_timer()
                print 'The code for %s run for %.2f seconds ' % (single_file, end_time - start_time)
        print "There are {} different chromosomes' data found".format(current_index - 1)

    writer.save()



if __name__ == '__main__':
    module_1(file_name='%sVU1.vcf_traps_vari_output' % ROOT_COMMON_INPUT)
