# FreqInPoP

FreqInPop (Frequencies of transmembrane protien variants in different Populations) is a python module that analyzes row-wise the output files from TraPS-VarI v0.0.2 and extracts mafs if found in the library_ph3 folder.